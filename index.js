const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

// routes
const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js");


const app = express();

// MongoDB Connection
mongoose.connect("mongodb+srv://chrrylpngnbn:123chrrylpngnbn@wdc028-course-booking.baoso.mongodb.net/b190-Course-Booking?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the database"));

app.use(cors());


app.use(express.json());
app.use(express.urlencoded({extended:true}))


app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

/*
  Heroku deployment
    Procfile - needed file in heroku to determine the command that will be used when staring the server (web: node app)
*/


// process.env.PORT handles the environment of the hosting websites should app bee hosted in a website such as Heroku
app.listen(process.env.PORT||4000,() => {console.log(`API now online at port ${process.env.PORT||4000}`)})