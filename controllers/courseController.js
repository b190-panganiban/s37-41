const Course = require("../models/Course.js");

// Creates new course
/*
    Miniactivity
        - create a new Course object using the mongoose model and the information from the request body {name, description, price}
        - save the new Course object to the database
        - send the Postman output in the google chat
        - 10 minutes (8:20 pm)

    // c/o Sir Chano
        module.exports.addCourse = (userData, courseData) => 
        {
            if (userData.isAdmin) 
            {
                let newCourse = new Course({
                    name: courseData.name,
                    description: courseData.description,
                    price: courseData.price
                });
                return newCourse.save().then((course, error) =>
                {
                    if (error) 
                    {
                        console.log(error)
                        return false;
                    } 
                    else 
                    {
                        return course;
                    }
                })
            } 
            else 
            {
                return Promise.reject('Unauthorised user');
            }
        }

*/
module.exports.addCourse = (data) => {
        let newCourse = new Course({
            name : data.name,
            description : data.description,
            price : data.price
        });
        return newCourse.save().then((course, error) => {
            if (error) {
                return false;
            } else {
                return true;
            };
        });
};






// retrieve all courses
/*
    Miniactivity
        1. retrieve all courses from the database and return the result

        5 minutes: 6:21 pm
*/
module.exports.getAllCourses = () =>{
    return Course.find({  }).then(result =>{
        return result;
    })
};

// retrieving all active courses
/*
    Miniactivity
        1. retrieve all courses from databases with the property of "isActive" to true
        2. return the result

        5 minutes: 6:32 pm (send the Postman output in the google chat)
*/
module.exports.getAllActive = () =>{
    return Course.find({ isActive: true }).then(result =>{
        return result;
    })
};

// retrieving a specific course
/*
    Miniactivity 
        1. Retrieve the course that match the course Id provided in the URL
        2. return the result

        10 minutes: 6:52 pm (send the ss of Postman Output in the google chat)
*/
module.exports.getCourse = (reqParams) =>{
    return Course.findById(reqParams.courseId).then(result => {
        return result;
    })
};

// update a course
/*
    Miniactivity:
        1. Create a variable "updateCourse" which will contain the information from the requestBody
        2. Find and update the course using the courseId found in the request params and the variable "updateCourse" containing the information from the requestBody (findByIdAndUpdate)
        3. return false if there are errors, true if the updating is successful

        10 minutes: 7:56 pm (send the output ss in the google chat)
*/
module.exports.updateCourse = ( reqParams, reqBody ) => {
    let updateCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }
    // findByIdAndUpdate - its purpose is to find a specific id in the database (first parameter) and update it using the information coming from the request body (second parameter)
    /*
        findByIdAndUpdate(documentId, updatesToBeApplied)
    */
    return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, err)=>{
        if (err) {
            return false;
        }else{
            return true;
        }
    })
};

// S40 ACTIVITY
// Archive a course
/*
    1. create an object "updateActiveField" and store isActive: false inside
    2. Find and update the course using the courseId found in the request params and the variable "updateActiveField" 
    3. return false if there are errors, true if the updating is successful
*/
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently
module.exports.archiveCourse = (reqParams, reqBody) => {
    let updateActiveField = {
        isActive : false
    };
    return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    });
};