const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

// Route for checking if the user's email already exists in the database
// since we will pass a "body" from the request object, post method will be used as HTTP method even if the goal is the just check the database if there is a user email save in it
router.post('/checkEmail', ( req, res ) =>{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
} )

// route for user registration
/*
	pass the req.body that has the contents of the user credentials to be saved in our database
	 call the function "registerUser" from our userController
*/
router.post("/register", ( req, res ) => {
	userController.registerUser( req.body ).then(resultFromController => res.send(resultFromController));
} );


// route for user authentication
router.post("/login", ( req, res ) => {
	userController.loginUser( req.body ).then( resultFromController => res.send( resultFromController ) );
} );

// S37-Activity
// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	// 
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// route for user enrollment
router.post('/enroll', auth.verify, (req, res) =>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})




module.exports = router;